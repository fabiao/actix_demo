mod api;
mod errors;
mod jwt_session;
mod models;
mod results;

use std::{env, time::Duration};

use actix_web::{get, middleware, web, App, HttpResponse, HttpServer, Responder};
use migration::{Migrator, MigratorTrait};
use sea_orm::{ConnectOptions, Database, DatabaseConnection};
use serde::Serialize;

#[derive(Serialize)]
pub struct Response {
    pub message: String,
}

#[get("/health")]
async fn healthcheck() -> impl Responder {
    let response = Response {
        message: "Everything is working fine".to_string(),
    };
    HttpResponse::Ok().json(response)
}

async fn not_found() -> results::CustomResult<HttpResponse> {
    let response = Response {
        message: "Resource not found".to_string(),
    };
    Ok(HttpResponse::NotFound().json(response))
}

async fn get_db_connection(db_url: String) -> results::CustomResult<DatabaseConnection> {
    let mut opt = ConnectOptions::new(db_url);
    opt.max_connections(100)
        .min_connections(5)
        .connect_timeout(Duration::from_secs(8))
        .acquire_timeout(Duration::from_secs(8))
        .idle_timeout(Duration::from_secs(8))
        .max_lifetime(Duration::from_secs(8))
        .sqlx_logging(true)
        .sqlx_logging_level(log::LevelFilter::Info);

    let conn = Database::connect(opt).await?;
    Migrator::up(&conn, None).await?;
    Ok(conn)
}

#[derive(Debug, Clone)]
pub struct AppState {
    pub db_connection: DatabaseConnection,
    pub jwt_secret: String,
    pub jwt_expires_in: String,
    pub jwt_maxage: i64,
}

#[actix_rt::main]
async fn main() -> std::io::Result<()> {
    env::set_var("RUST_LOG", "debug");
    env_logger::init();

    dotenv::dotenv().ok();
    env::set_var("RUST_LOG", "actix_web=debug");

    let db_url = env::var("DATABASE_URL").expect("DATABASE_URL is not set in .env file");
    let db_connection = get_db_connection(db_url).await.unwrap();
    let app_state = AppState {
        db_connection: db_connection,
        jwt_secret: env::var("JWT_SECRET").expect("JWT_SECRET must be set"),
        jwt_expires_in: env::var("JWT_EXPIRED_IN").expect("JWT_EXPIRED_IN must be set"),
        jwt_maxage: env::var("JWT_MAXAGE")
            .expect("JWT_MAXAGE must be set")
            .parse::<i64>()
            .unwrap(),
    };

    // Start http server
    let server = HttpServer::new(move || {
        App::new()
            .app_data(web::Data::new(app_state.clone()))
            .configure(api::config)
            .service(healthcheck)
            .default_service(web::route().to(not_found))
            .wrap(middleware::Logger::default())
    });

    let host = env::var("HOST").expect("HOST is not set in .env file");
    let port = env::var("PORT").expect("PORT is not set in .env file").parse().unwrap();
    log::info!("Actix listening on {}:{}", host, port);
    server.bind((host, port))?.run().await
}
