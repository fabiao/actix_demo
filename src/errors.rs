use core::fmt;
use std::{env, error::Error as StdError};

use actix_web::{error::ResponseError, http::StatusCode, HttpResponse};
use sea_orm::DbErr;

#[derive(Debug, PartialEq, Eq, Clone)]
pub struct HttpStatusCodeMessageError {
    pub status: StatusCode,
    pub message: String,
}

impl StdError for HttpStatusCodeMessageError {
    fn description(&self) -> &str {
        &self.message
    }
}

impl fmt::Display for HttpStatusCodeMessageError {
    fn fmt(&self, f: &mut fmt::Formatter) -> fmt::Result {
        write!(f, "{:?}", self.message)
    }
}

#[derive(thiserror::Error, Debug)]
pub enum CustomError {
    #[error("an error occurred: {0}")]
    StatusCodeMessageError(#[from] HttpStatusCodeMessageError),
    #[error("an error occurred: {0}")]
    AnyhowError(#[from] anyhow::Error),
    #[error("an error occurred: {0}")]
    ActixError(#[from] actix_web::Error),
    #[error("an error occurred: {0}")]
    VarError(#[from] env::VarError),
    #[error("an error occurred: {0}")]
    JsonWebTokenError(#[from] jsonwebtoken::errors::Error),
    #[error("a db error occurred: {0}")]
    DbError(#[from] sea_orm::DbErr),    
}

impl CustomError  {
    pub fn new(status_code: StatusCode, message: String) -> Self {
        Self::StatusCodeMessageError(HttpStatusCodeMessageError {
            status: status_code,
            message: message,
        })
    }
}

impl ResponseError for CustomError {
    fn status_code(&self) -> StatusCode {
        match &self {
            Self::StatusCodeMessageError(error) => error.status,
            Self::AnyhowError(_) => StatusCode::INTERNAL_SERVER_ERROR,
            Self::ActixError(error) => error.as_response_error().status_code(),
            Self::VarError(_) => StatusCode::INTERNAL_SERVER_ERROR,
            Self::JsonWebTokenError(_) => StatusCode::BAD_REQUEST,
            Self::DbError(error) => {
                match error {
                    DbErr::RecordNotFound(_message) => StatusCode::NOT_FOUND,
                    DbErr::AttrNotSet(_message) => StatusCode::BAD_REQUEST,
                    DbErr::Type(_message) => StatusCode::BAD_REQUEST,
                    DbErr::Json(_message) => StatusCode::BAD_REQUEST,
                    DbErr::RecordNotInserted => StatusCode::BAD_REQUEST,
                    DbErr::RecordNotUpdated => StatusCode::BAD_REQUEST,
                    /*DbErr::ConnectionAcquire => StatusCode::INTERNAL_SERVER_ERROR,
                    DbErr::Conn(_error) => StatusCode::INTERNAL_SERVER_ERROR,
                    DbErr::Exec(_error) => StatusCode::INTERNAL_SERVER_ERROR,
                    DbErr::Query(_error) => StatusCode::INTERNAL_SERVER_ERROR,
                    DbErr::ConvertFromU64(_) => StatusCode::INTERNAL_SERVER_ERROR,
                    DbErr::UnpackInsertId => StatusCode::INTERNAL_SERVER_ERROR,
                    DbErr::UpdateGetPrimaryKey => StatusCode::INTERNAL_SERVER_ERROR,
                    DbErr::Custom(_message) => StatusCode::INTERNAL_SERVER_ERROR,
                    DbErr::Migration(_message) => StatusCode::INTERNAL_SERVER_ERROR,*/
                    _ => StatusCode::INTERNAL_SERVER_ERROR,
                }
            }
        }
    }

    fn error_response(&self) -> HttpResponse {
        HttpResponse::build(self.status_code()).body(self.to_string())
    }
}
