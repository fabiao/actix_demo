use std::env;

use actix_web::{dev, FromRequest, HttpRequest, http::StatusCode};
use chrono::{Utc, Duration};
use entity::user::Model;
use futures::future::{err, ok, Ready};
use jsonwebtoken::{decode, Algorithm, DecodingKey, Validation, Header, encode, EncodingKey};
use serde::{Deserialize, Serialize};

use crate::errors::CustomError;

#[derive(Debug, Serialize, Deserialize)]
pub struct TokenClaims {
    pub sub: String,
    pub exp: usize,
    pub iat: usize,
}

pub struct AuthorizationService;


pub fn generate_token(jwt_maxage: i64, jwt_secret: &[u8], user: &Model) -> Result<String, CustomError> {
    let now = Utc::now();
    let iat = now.timestamp() as usize;
    let exp = (now + Duration::minutes(jwt_maxage)).timestamp() as usize;
    let claims = TokenClaims {
        sub: user.id.to_string(),
        exp,
        iat,
    };

    let token = encode(
        &Header::default(),
        &claims,
        &EncodingKey::from_secret(jwt_secret),
    )?;
    Ok(token)
}

impl FromRequest for AuthorizationService {
    type Error = CustomError;
    type Future = Ready<Result<Self, Self::Error>>;

    fn from_request(req: &HttpRequest, _payload: &mut dev::Payload) -> Self::Future {
        if let Some(auth) = req.headers().get("Authorization") {
            if let Ok(auth) = auth.to_str() {
                let tokens: Vec<&str> = auth.split(' ').map(|t| t.trim()).collect();
                if tokens.len() == 2 && tokens[0] == "Bearer" {
                    let token = tokens[1];
                    let secret_key = env::var("JWT_SECRET");
                    return match secret_key {
                        Ok(key) => {
                            match decode::<TokenClaims>(
                                token,
                                &DecodingKey::from_secret(key.as_bytes()),
                                &Validation::new(Algorithm::HS256),
                            ) {
                                Ok(_token) => ok(AuthorizationService),
                                Err(e) => err(CustomError::new(StatusCode::UNAUTHORIZED, String::from(e.to_string()))),
                            }
                        }
                        Err(e) => 
                        err(CustomError::new(StatusCode::UNAUTHORIZED, String::from(e.to_string())))
                    };
                } else {
                    return err(CustomError::new(StatusCode::UNAUTHORIZED, String::from("Invalid authorization token format")));
                }
            } 
        }

        err(CustomError::new(StatusCode::UNAUTHORIZED, String::from("Invalid authorization token")))
    }
}
