use crate::errors::CustomError;

// Short hand alias, which allows you to use just Result<T>
pub type CustomResult<T> = std::result::Result<T, CustomError>;
