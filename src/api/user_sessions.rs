use actix_web::{post, web, HttpResponse, http::StatusCode};
use entity::user;
use sea_orm::{EntityTrait, ColumnTrait, QueryFilter};

use crate::{AppState, models::users::Login, results, jwt_session::generate_token, errors::CustomError};



#[post("/login")]
pub async fn login(app_state: web::Data<AppState>, item: web::Json<Login>) -> results::CustomResult<HttpResponse> {
    if let Some(user) = user::Entity::find()
        .filter(user::Column::Email.like(&item.username))
        .one(&app_state.db_connection)
        .await? {
            let token = generate_token(app_state.jwt_maxage, app_state.jwt_secret.as_ref(), &user)?;
            return Ok(HttpResponse::Ok().json(token));
        }

    Err(CustomError::new(StatusCode::NOT_FOUND, String::from("User not found")))
}