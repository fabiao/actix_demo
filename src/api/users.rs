use actix_web::{delete, get, http::StatusCode, patch, post, web, HttpResponse};
use chrono::Local;
use entity::user;
use sea_orm::{ActiveModelTrait, EntityTrait, Set};

use crate::{
    errors::CustomError,
    jwt_session,
    models::users::{InputUser, User},
    results, AppState,
};

#[get("/users")]
pub async fn get_users(
    app_state: web::Data<AppState>,
    _auth_service: jwt_session::AuthorizationService,
) -> results::CustomResult<HttpResponse> {
    let users = user::Entity::find().all(&app_state.db_connection).await?;
    let users: Vec<User> = users.iter().map(|u| User::from(u)).collect();
    Ok(HttpResponse::Ok().json(users))
}

#[get("/user/{id}")]
pub async fn get_user_by_id(
    app_state: web::Data<AppState>,
    id: web::Path<i32>,
    _auth_service: jwt_session::AuthorizationService,
) -> results::CustomResult<HttpResponse> {
    let user = user::Entity::find_by_id(id.into_inner())
        .one(&app_state.db_connection)
        .await?;

    match user {
        Some(user) => Ok(HttpResponse::Ok().json(User::from(&user))),
        None => Err(CustomError::new(
            StatusCode::NOT_FOUND,
            String::from("User not found"),
        )),
    }
}

#[post("/user")]
pub async fn add_user(
    app_state: web::Data<AppState>,
    item: web::Json<InputUser>,
) -> results::CustomResult<HttpResponse> {
    let user = user::ActiveModel {
        first_name: Set(item.first_name.clone()),
        last_name: Set(item.last_name.clone()),
        email: Set(item.email.clone()),
        created_at: Set(Local::now().naive_utc()),
        ..Default::default()
    };
    let user = user.insert(&app_state.db_connection).await?;
    Ok(HttpResponse::Ok().json(User::from(&user)))
}

#[patch("/user/{id}")]
pub async fn update_user(
    app_state: web::Data<AppState>,
    id: web::Path<i32>,
    item: web::Json<InputUser>,
    _auth_service: jwt_session::AuthorizationService,
) -> results::CustomResult<HttpResponse> {
    let user = user::Entity::find_by_id(id.into_inner())
        .one(&app_state.db_connection)
        .await?;
    match user {
        Some(user) => {
            let mut user: user::ActiveModel = user.into();
            user.first_name = Set(item.first_name.clone());
            user.last_name = Set(item.last_name.clone());
            user.email = Set(item.email.clone());
            user.updated_at = Set(Some(Local::now().naive_utc()));
            let user = user.update(&app_state.db_connection).await?;
            Ok(HttpResponse::Ok().json(User::from(&user)))
        }
        None => Err(CustomError::new(
            StatusCode::NOT_FOUND,
            String::from("User not found"),
        )),
    }
}

#[delete("/user/{id}")]
pub async fn delete_user(
    app_state: web::Data<AppState>,
    id: web::Path<i32>,
    _auth_service: jwt_session::AuthorizationService,
) -> results::CustomResult<HttpResponse> {
    let user = user::Entity::find_by_id(id.into_inner())
        .one(&app_state.db_connection)
        .await?;
    match user {
        Some(user) => {
            let user: user::ActiveModel = user.into();
            let res = user.delete(&app_state.db_connection).await?;
            Ok(HttpResponse::Ok().json(res.rows_affected))
        }
        None => Err(CustomError::new(
            StatusCode::NOT_FOUND,
            String::from("User not found"),
        )),
    }
}
