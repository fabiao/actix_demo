
use actix_web::web;

pub mod user_sessions;
pub mod users;

pub fn config(cfg: &mut web::ServiceConfig) {
    cfg.service(
        web::scope("/api")
            .service(users::get_users)
            .service(users::get_user_by_id)
            .service(users::add_user)
            .service(users::update_user)
            .service(users::delete_user)
            .service(user_sessions::login)
    );
}