use chrono::NaiveDateTime;
use entity::user::Model;
use serde::{Serialize, Deserialize};

#[derive(Debug, Serialize, Deserialize)]
pub struct Login {
    pub username: String,
    pub password: String,
}

#[derive(Debug, Serialize, Deserialize)]
pub struct InputUser {
    pub first_name: String,
    pub last_name: String,
    pub email: String,
}

#[derive(Debug, Serialize, Deserialize)]
pub struct User {
    pub id: i32,
    pub first_name: String,
    pub last_name: String,
    pub email: String,
    pub created_at: NaiveDateTime,
    pub updated_at: Option<NaiveDateTime>,
}

impl From<&Model> for User {
    fn from(model: &Model) -> User {
        User {
            id: model.id,
            first_name: model.first_name.clone(),
            last_name: model.last_name.clone(),
            email: model.email.clone(),
            created_at: model.created_at,
            updated_at: model.updated_at
        }
    }
}